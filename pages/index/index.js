//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    dengji: ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten']
  },
  onLoad: function () {

    var that = this;

    console.log(app.globalData.userInfo);
    if (app.globalData.userInfo) {

      this.setData({
        userInfo: app.globalData.userInfo
      })

      console.log(app.globalData.userInfo);
    }
    else {
      app.userInfoReadyCallback = res => {
        console.log(res);
        that.setData({
          userInfo: app.globalData.userInfo
        })
      }
    }
  },

  onShow () {
    // 获取用户信息
    var that = this;
    wx.request({
      url: 'https://miniapp.okaoyan.com/api/v1/user/info',
      header: {
        SessionKey: wx.getStorageSync('code_secret_key')
      },
      success(res) {
        app.globalData.userInfo = res.data.data;
        that.setData({
          userInfo: app.globalData.userInfo
        })
      }
    })
  }
})
